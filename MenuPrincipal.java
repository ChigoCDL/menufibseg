import java.util.Scanner;
class MenuPrincipal {
  public static void main(String...Args){
    Scanner lee=new Scanner(System.in);
    int opc;
    do{
      System.out.println("*****\tMENU\t*****");
      System.out.println("1. Serie Fibonacci");
      System.out.println("2. Horas en determinados segundos");
      System.out.println("3. Salir");
      System.out.print("Ingresa la opcion deseada: ");
      opc=lee.nextInt();
      switch(opc){
        case 1:
          System.out.println("Cuantos numeros quieres ver de la serie?");
          Operaciones.fibonacci(lee.nextInt());
          break;
        case 2:
          System.out.println("Ingresa la cantidad de segundos");
          Operaciones.segundos(lee.nextInt());
          break;
        case 3:
          System.out.println("Bye!");
          break;
        default:
          System.out.println("\tOpcion no valida!");
      }
    }while(opc!=3);
  }
}
